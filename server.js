const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const mongoose = require('mongoose')

const { port } = require('./Config/server.config')
const { dbURI } = require('./Config/data.config')

// import router
const authRouter = require('./router/user.router')
const postRouter = require('./router/posts.router')
var app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

dotenv.config()
// Connect data
mongoose.connect(dbURI, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true, useUnifiedTopology: true })
  .then(() => console.log('Database connected...'))
  .catch(error => console.log('Can not connect to database: ', error.message))

// Router middlewares
app.use('/api/users', authRouter)
app.use('/api/posts', postRouter)

// Handle error
process.on('uncaughtException', (err, origin) => {
  console.log(`Caught exception: ${err}`)
  console.log(`Exception origin: ${origin}`)
})

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', promise, 'reason:', reason)
})

// Call server
app.listen(port, () => console.log(`Server running on port: ${port}`))

module.exports = app
